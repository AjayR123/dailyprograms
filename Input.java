package Sajinclass;

import java.util.Scanner;

class Input {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("enter number");
		int number = scanner.nextInt();
		int count = 0;
		if (number == 0) {
			count++;
		}
		while (number != 0) {
			number = number / 10;
			count++;
		}
		System.out.println("number of digits=" + count);

	}

}
